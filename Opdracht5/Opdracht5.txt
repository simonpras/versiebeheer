Studentnummer: 332959
Naam: Simon Pras

Verschilt de Terminal in Visual Studio Code met Git Bash?
	# de paden in de terminal zijn heel moeilijk.

Waar worden Branches vaak voor gebruikt?
	# om nieuwe zaken te testen.

Hoe vaak ben je in Opdracht 5A van Branch gewisseld?
	# 2 keer.

Vul de volgende commando's aan:
 -Checken op welke branch je aan het werken bent:
	# git branch
 -Nieuwe Branch aanmaken
	# git branch -d Opdracht5

 -Van Branch wisselen
	# git checkout Opdracht5
 -Branch verwijderen
	# git branch -d Opdracht5 
